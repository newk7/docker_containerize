FROM python:3.5-alpine3.8

LABEL maintainer='ian.newcombe@libertymutual.com'

COPY * /app/
RUN pip3 install Flask
EXPOSE 8080

RUN apk add --no-cache curl

ENV ENVIRONMENT DEV
ENV DISPLAY_FONT arial
ENV DISPLAY_COLOR blue

#TODO *bonus* add a health check that tells docker the app is running properly
HEALTHCHECK --interval=1m --timeout=3s \
  CMD curl -f http://localhost:8080 || exit 1

# TODO have the app run as a non-root user
RUN chown -R 1007 /app
USER 1007

CMD python /app/app.py
